#pragma once
#include "Resourses.h"
#include <iostream>
#include <iomanip>

void printfAllHashCodes()
{
	for (auto& i : allCode)
	{
		for (auto& j : i)
		{
			std::cout 
				<< "TokenWord = " << std::setw(10) << std::left << j.word
				<< "TokenHashCode = " << std::setw(10) << std::left << j.hashCode
				<< "TokenLine = " << std::setw(10) << std::left << j.line
				<< "TokenColumn = " << std::setw(10) << std::left << j.column
				<< std::endl;

			fileOutput
				<< "TokenWord = " << std::setw(10) << std::left << j.word
				<< "TokenHashCode = " << std::setw(10) << std::left << j.hashCode
				<< "TokenLine = " << std::setw(10) << std::left << j.line
				<< "TokenColumn = " << std::setw(10) << std::left << j.column
				<< std::endl;
		}
	}
}
