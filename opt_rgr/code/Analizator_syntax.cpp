#include "Analizator.h"
#include "Resourses.h"

extern std::ifstream fileInput;

int line = 1;
int column = 0;

int openedCommentLine;
int openedCommentColumn;

bool isDeclaretionListOpenned = false;

std::vector<std::vector<lexem>> allCode;

std::pair<int, int> getPosittionErrorSyntax()
{
	return std::make_pair(line, column);
}

char getNextChar()
{
	char temp = fileInput.get();
	if (temp != -1)
	{
		++column;
		return temp;
	}

	return 0;
}

int analizator_syntax()
{
	char nextChar;

	int error = -1;

	allCode.push_back(std::vector<lexem>());

	while (nextChar = getNextChar())
	{
		if ((error = createLexem(nextChar)) != 0) break;
	}

	return error;
}

int createLexem(char character)
{
	if (std::find(numerals.begin(), numerals.end(), character) != numerals.end())
	{
		lexem tempLexem;
		tempLexem.word.push_back(character);
		tempLexem.line = line;
		tempLexem.column = column;

		char tempNextChar = getNextChar();

		while (std::find(numerals.begin(), numerals.end(), tempNextChar) != numerals.end())
		{
			tempLexem.word.push_back(tempNextChar);

			tempNextChar = getNextChar();
		}

		if (tempNextChar != '#')
		{
			return 6;
		}

		tempLexem.word.push_back(tempNextChar);

		tempNextChar = getNextChar();

		if (std::find(numerals.begin(), numerals.end(), tempNextChar) == numerals.end())
		{
			return 7;
		}

		while (std::find(numerals.begin(), numerals.end(), tempNextChar) != numerals.end())
		{
			tempLexem.word.push_back(tempNextChar);

			tempNextChar = getNextChar();
		}

		tempLexem.hashCode = getNumberOfHashCodeSpecialLexem(tempLexem.word);
		tempLexem.type = TYPE_IDENTIFIER;

		allCode[allCode.size() - 1].push_back(tempLexem);


		return createLexem(tempNextChar);
	}

	if (std::find(lettersBig.begin(), lettersBig.end(), character) != lettersBig.end() ||
		std::find(lettersSmall.begin(), lettersSmall.end(), character) != lettersSmall.end())
	{
		lexem tempLexem;
		tempLexem.word.push_back(character);
		tempLexem.line = line;
		tempLexem.column = column;

		char tempNextChar = getNextChar();

		while (std::find(lettersBig.begin(), lettersBig.end(), tempNextChar) != lettersBig.end() ||
			std::find(lettersSmall.begin(), lettersSmall.end(), tempNextChar) != lettersSmall.end() ||
			std::find(numerals.begin(), numerals.end(), tempNextChar) != numerals.end())
		{
			tempLexem.word.push_back(tempNextChar);

			tempNextChar = getNextChar();
		}

		if ((tempLexem.hashCode = getNumberOfHashCodeReservedWords(tempLexem.word)) != -1)
		{
			if (tempLexem.hashCode < freeHashCode_reservedWords)
			{
				tempLexem.type = TYPE_ATTRIBUTES;
			}
			else
			{
				tempLexem.type = TYPE_RESERVED_WORDS;
			}

			allCode[allCode.size() - 1].push_back(tempLexem);

			return createLexem(tempNextChar);
		}

		tempLexem.hashCode = getNumberOfHashCodeIdentifier(tempLexem.word);
		tempLexem.type = TYPE_IDENTIFIER;

		allCode[allCode.size() - 1].push_back(tempLexem);

		return createLexem(tempNextChar);
	}

	if (std::find(delimitersUsed.begin(), delimitersUsed.end(), character) != delimitersUsed.end())
	{
		allCode[allCode.size() - 1].push_back(lexem());

		auto& tempLexem = allCode[allCode.size() - 1][allCode[allCode.size() - 1].size() - 1];

		tempLexem.column = column;
		tempLexem.line = line;
		tempLexem.word = character;
		tempLexem.hashCode = getNumberOfHashCodeDelimiter(character);
		tempLexem.type = TYPE_DELIMITERS;

		return 0;
	}

	if (character == '(')
	{
		char tempNextChar;
		if ((tempNextChar = getNextChar()) == '*')
		{
			openedCommentLine = line;
			openedCommentColumn = column - 1;

			tempNextChar = getNextChar();

			while (true) 
			{
				if (tempNextChar == '*')
				{
					if ((tempNextChar = getNextChar()) == ')')
					{
						return 0;
					}
				}
				else
				{
					if (tempNextChar == '\0')
					{
						line = openedCommentLine;
						column = openedCommentColumn;

						return 5;
					}

					if (tempNextChar == '\n')
					{
						column = 0;
						++line;
						allCode.push_back(std::vector<lexem>());
					}

					tempNextChar = getNextChar();
				}
			}
		}
		else
		{
			allCode[allCode.size() - 1].push_back(lexem());

			auto& tempLexem = allCode[allCode.size() - 1][allCode[allCode.size() - 1].size() - 1];

			tempLexem.column = column - 1;
			tempLexem.line = line;
			tempLexem.word = character;
			tempLexem.hashCode = 410;
			tempLexem.type = TYPE_DELIMITERS;

			return createLexem(tempNextChar);
		}
	}

	if (character == ')')
	{
		allCode[allCode.size() - 1].push_back(lexem());

		auto& tempLexem = allCode[allCode.size() - 1][allCode[allCode.size() - 1].size() - 1];

		tempLexem.column = column;
		tempLexem.line = line;
		tempLexem.word = character;
		tempLexem.hashCode = 411;
		tempLexem.type = TYPE_DELIMITERS;

		return 0;
	}

	if (std::find(delimitersOther.begin(), delimitersOther.end(), character) != delimitersOther.end())
	{
		char tempNextChar;
		while (std::find(delimitersOther.begin(), delimitersOther.end(), (tempNextChar = getNextChar())) != delimitersOther.end())
		{
			
		}

		return createLexem(tempNextChar);
	}

	if (character == '\n')
	{
		column = 0;
		++line;
		allCode.push_back(std::vector<lexem>());

		return 0;
	}

	if (character == '\0')
	{
		return 0;
	}

	return 1; //unused character
}
