#pragma once
#include "Characters.h"
#include "Resourses.h"
#include "TreeLexem.h"
#include <fstream>
#include <iostream>

extern std::ifstream fileInput;
extern std::ofstream fileOutput;
int analizator_syntax();
char getNextChar();
int createLexem(char character);
std::pair<int, int> getPosittionErrorSyntax();

int analizator_lexem();

namespace FLexem
{
	int SignalProgram(int indent, appleLexem& member);
	int Program(int indent, appleLexem& member);
	int Block(int indent, appleLexem& member);
	int StatementsList(int indent, appleLexem& member);
	int ParametrsList(int indent, appleLexem& member);
	int DeclarationsList(int indent, appleLexem& member);
	int Declaration(int indent, appleLexem& member);
	int IdentifiersList(int indent, appleLexem& member);
	int AttributesList(int indent, appleLexem& member);
	bool Attribute(int indent, appleLexem& member);
	bool ProcedureIdentifier(int indent, appleLexem& member);
	bool VariableIdentifier(int indent, appleLexem& member);
};

std::pair<int, int> getPosittionErrorLexem();
