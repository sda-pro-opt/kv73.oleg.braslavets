#pragma once

#include <string>
#include <vector>

struct lexem
{
	int type;
	int column;
	int line;
	std::string word;
	int hashCode;
};

#define TYPE_IDENTIFIER 0
#define TYPE_ATTRIBUTES 1
#define TYPE_RESERVED_WORDS 2
#define TYPE_DELIMITERS 3
#define TYPE_SPECIAL_LEXEM 4

extern std::vector<std::vector<lexem>> allCode;

static int freeHashCode_specialLexem = 0; //speciallexem
static int freeHashCode_identifiers = 100; //��������������
const int freeHashCode_attributes = 200; //���� ������
const int freeHashCode_reservedWords = 300; //����������������� �����
const int freeHashCode_delimiters = 400; //�������������� ����� ������ ����� �������������� � ���������

inline int getNumberOfHashCodeDelimiter(char element)
{
	for (int i = 0; i < delimitersUsed.size(); ++i)
	{
		if (delimitersUsed[i] == element) return freeHashCode_delimiters + i;
	}

	return -1;
}

inline int getNumberOfHashCodeReservedWords(std::string element)
{
	for (int i = 0; i < reservedWords.size(); ++i)
	{
		if (reservedWords[i] == element)
		{
			if (i < 4)
			{
				return (freeHashCode_reservedWords + i);
			}
			else
			{
				return (freeHashCode_attributes + i - 4);
			}
		}
	}

	return -1;
}

inline int getNumberOfHashCodeIdentifier(std::string element)
{
	for (auto i : allCode)
	{
		for (auto j : i)
		{
			if (j.word == element)
			{
				return j.hashCode;
			}
		}
	}

	return freeHashCode_identifiers++;
}

inline int getNumberOfHashCodeSpecialLexem(std::string element)
{
	for (auto i : allCode)
	{
		for (auto j : i)
		{
			if (j.word == element)
			{
				return j.hashCode;
			}
		}
	}

	return freeHashCode_specialLexem++;
}
