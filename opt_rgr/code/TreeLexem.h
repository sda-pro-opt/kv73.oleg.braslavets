#pragma once
#include "Resourses.h"
#include "Analizator.h"
#include <iostream>
#include <fstream>

#define indentSize 5
extern std::ofstream fileOutput;

inline void printfIndent(int indent)
{
	for (int i = 0; i < indent; ++i)
	{
		if (i % indentSize == 0)
		{
			std::cout << "|";
			fileOutput << "|";
		}
		std::cout << " ";
		fileOutput << " ";
	}
}

struct appleLexem
{
	bool isLexem;
	lexem data;
	int typeRootName = -1;
	std::vector<appleLexem> childs;
	int indent;
};

class TreeLexem
{
public:
	appleLexem root;

    static void printTree(appleLexem root)
    {
		printfIndent(root.indent);
		if (root.isLexem == true)
		{
			std::cout << std::to_string(root.data.hashCode) + " " + root.data.word << std::endl;
			fileOutput << std::to_string(root.data.hashCode) + " " + root.data.word << std::endl;
		}
		else
		{
			std::cout << typeRootNames[root.typeRootName - 1] << std::endl;
			fileOutput << typeRootNames[root.typeRootName - 1] << std::endl;
		}
        for (auto& i : root.childs)
        {
			printTree(i);
        }
    }
};