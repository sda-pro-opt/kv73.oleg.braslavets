#pragma once

#include <vector>
#include <string>

const std::vector<char> lettersBig = {  'A', 'B', 'C', 'D', 
								  'E', 'F', 'G', 'H', 
								  'I', 'J', 'K', 'L', 
								  'M', 'N', 'O', 'P', 
								  'Q', 'R', 'S', 'T', 
								  'U', 'V', 'W', 'X', 
								  'Y', 'Z'};

const std::vector<char> lettersSmall = {  'a', 'b', 'c', 'd',
									'e', 'f', 'g', 'h',
									'i', 'j', 'k', 'l',
									'm', 'n', 'o', 'p',
									'q', 'r', 's', 't',
									'u', 'v', 'w', 'x',
									'y', 'z' };

const std::vector<char> numerals = {'0', '1', '2', '3',
							  '4', '5', '6', '7',
							  '8', '9', };

const std::vector<char> delimitersUsed = {
	';',			//400
	'.',			//401
	':',			//402
	',',			//403
	//'(',			//410
	//')'			//411
};

const std::vector<char> delimitersOther = { ' ', '	'};

const std::vector<std::string> reservedWords = {
	"PROGRAM",		//300
	"PROCEDURE",	//301
	"BEGIN",		//302
	"END",			//303

	"SIGNAL",		//200
	"COMPLEX",		//201
	"INTEGER",		//202
	"FLOAT",		//203
	"BLOCKFLOAT",	//204
	"EXT"			//205
};

const std::vector<std::string> typeRootNames = {
	"<signal-program>",			//1
	"<program>",				//2
	"<block>",					//3
	"<statements-list>",		//4
	"<parameters-list>",		//5
	"<declarations-list>",		//6
	"<declaration>",			//7
	"<identifiers-list>",		//8
	"<attributes-list>",		//9
	"<attribute>",				//10
	"<procedure-identifier>",	//11
	"<variable-identifier>",	//12
	"<empty>"					//13
};
