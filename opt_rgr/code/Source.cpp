#include "Analizator.h"
#include "Errors.h"
#include "utility.h"

std::ifstream fileInput;
std::ofstream fileOutput;

int main()
{
	setlocale(LC_CTYPE, "rus");

	std::string pathToFiles;

	std::cout << "����i�� ���� �� �����" << std::endl;

	std::getline(std::cin, pathToFiles);

	std::string pathToInput(pathToFiles);
	std::string pathToOutput(pathToFiles);

	pathToOutput += "\\expected.txt";
	pathToInput += "\\input.sig";

	fileInput.open(pathToInput);

	if (fileInput.is_open() == false)
	{
		std::cout << "�� ������� �i������ ����, �����i��� ���� �� �����" << std::endl;
		system("pause");
		return 0;
	}

	fileOutput.open(pathToOutput);

	int result = analizator_syntax();

	if (result >= 1)
	{
		std::cout << errors_syntax[result - 1] << std::endl;
		fileOutput << errors_syntax[result - 1] << std::endl;

		std::cout << "line = " << getPosittionErrorSyntax().first << ", column = " << getPosittionErrorSyntax().second << std::endl;
		fileOutput << "line = " << getPosittionErrorSyntax().first << ", column = " << getPosittionErrorSyntax().second << std::endl;

		system("pause");

		return 0;
	}
	else
	{

		printfAllHashCodes();
	}

	std::cout << std::endl;

	result = analizator_lexem();
	if (result >= 1)
	{
		std::cout << errors_lexem[result - 1] << std::endl;
		fileOutput << errors_lexem[result - 1] << std::endl;

		std::cout << "line = " << getPosittionErrorLexem().first << ", column = " << getPosittionErrorLexem().second << std::endl;
		fileOutput << "line = " << getPosittionErrorLexem().first << ", column = " << getPosittionErrorLexem().second << std::endl;

		system("pause");

		return 0;
	}

	system("pause");

	return 0;
}
