﻿#include "Analizator.h"

TreeLexem treeOfLexem;

int counterNowLexem_line;
int counterNowLexem_column;

std::pair<int, int> getPosittionErrorLexem()
{
	return std::make_pair(counterNowLexem_line, counterNowLexem_column);
}

bool addColumn(int& counterNowLexem_line, int& counterNowLexem_column)
{
	bool haveLineLexems = true;
	size_t temp = allCode.size();
	while ((haveLineLexems = (temp - 1 >= counterNowLexem_line)) && allCode[counterNowLexem_line].size() == 0)
	{																	
		++counterNowLexem_line;
	}	

	if (haveLineLexems == false)
	{
		return false;
	}

	temp = allCode[counterNowLexem_line].size();
	if (temp - 1 > counterNowLexem_column)
	{																		
		++counterNowLexem_column;
	}																		
	else																	
	{																		
		counterNowLexem_column = 0;
		++counterNowLexem_line;
	}

	return true;
}

bool getLexem(int& counterNowLexem_line, int& counterNowLexem_column)
{
	int temp = (int)allCode.size();
	if (temp - 1 >= counterNowLexem_line)
	{
		temp = allCode[counterNowLexem_line].size();
		if (temp - 1 >= counterNowLexem_column)
		{
			return true;
		}
		else
		{
			return getLexem(++counterNowLexem_line, (counterNowLexem_column = 0));
		}
	}
	else
	{
		return false;
	}
}

int analizator_lexem()
{
	counterNowLexem_line = 0;
	counterNowLexem_column = 0;

	int error = FLexem::SignalProgram(0, treeOfLexem.root);

	if (error == 0)
	{
		if (counterNowLexem_line + 1 < allCode.size())
		{
			for (int i = counterNowLexem_line + 1; i < allCode.size(); ++i)
			{
				if (allCode[i].size() != 0)
				{
					return 8;
				}
			}
			return error;
		}
		if (counterNowLexem_line + 1 == allCode.size() && counterNowLexem_column < allCode[counterNowLexem_line].size())
		{
			return 8;
		}
	}

	treeOfLexem.printTree(treeOfLexem.root);

	return error;
}

int FLexem::SignalProgram(int indent, appleLexem& member)
{
	member.isLexem = false;
	member.indent = indent;
	member.typeRootName = 1;

	indent += indentSize;
	member.childs.emplace_back();
	return FLexem::Program(indent, member.childs[member.childs.size() - 1]);
}

int FLexem::Program(int indent, appleLexem& member)
{
	member.isLexem = false;
	member.indent = indent;
	member.typeRootName = 2;

	indent += indentSize;
	int error = 0;

	if (getLexem(counterNowLexem_line, counterNowLexem_column) == true && 
		allCode[counterNowLexem_line][counterNowLexem_column].hashCode == 300)
	{
		member.childs.emplace_back();
		member.childs[member.childs.size() - 1].isLexem = true;
		member.childs[member.childs.size() - 1].indent = indent;
		member.childs[member.childs.size() - 1].data = allCode[counterNowLexem_line][counterNowLexem_column];

		addColumn(counterNowLexem_line, counterNowLexem_column);

		member.childs.emplace_back();
		if (FLexem::ProcedureIdentifier(indent, member.childs[member.childs.size() - 1]) == false)
		{
			return 4;
		}

		if (getLexem(counterNowLexem_line, counterNowLexem_column) == true && 
			allCode[counterNowLexem_line][counterNowLexem_column].hashCode == 400)
		{
			member.childs.emplace_back();
			member.childs[member.childs.size() - 1].isLexem = true;
			member.childs[member.childs.size() - 1].indent = indent;
			member.childs[member.childs.size() - 1].data = allCode[counterNowLexem_line][counterNowLexem_column];

			addColumn(counterNowLexem_line, counterNowLexem_column);

			member.childs.emplace_back();
			if ((error = FLexem::Block(indent, member.childs[member.childs.size() - 1])) != 0)
			{
				return error;
			}

			if (getLexem(counterNowLexem_line, counterNowLexem_column) == true && 
				allCode[counterNowLexem_line][counterNowLexem_column].hashCode == 401)
			{
				member.childs.emplace_back();
				member.childs[member.childs.size() - 1].isLexem = true;
				member.childs[member.childs.size() - 1].indent = indent;
				member.childs[member.childs.size() - 1].data = allCode[counterNowLexem_line][counterNowLexem_column];

				addColumn(counterNowLexem_line, counterNowLexem_column);
				return 0;
			}
			else
			{
				return 3;
			}
		}
		else
		{
			return 2;
		}
	}
	if (getLexem(counterNowLexem_line, counterNowLexem_column) == true && 
		allCode[counterNowLexem_line][counterNowLexem_column].hashCode == 301)
	{
		member.childs.emplace_back();
		member.childs[member.childs.size() - 1].isLexem = true;
		member.childs[member.childs.size() - 1].indent = indent;
		member.childs[member.childs.size() - 1].data = allCode[counterNowLexem_line][counterNowLexem_column];

		addColumn(counterNowLexem_line, counterNowLexem_column);

		member.childs.emplace_back();
		if (FLexem::ProcedureIdentifier(indent, member.childs[member.childs.size() - 1]) == false)
		{
			return 4;
		}

		member.childs.emplace_back();
		if ((error = FLexem::ParametrsList(indent, member.childs[member.childs.size() - 1])) != 0)
		{
			return error;
		}

		if (getLexem(counterNowLexem_line, counterNowLexem_column) == true && 
			allCode[counterNowLexem_line][counterNowLexem_column].hashCode == 400)
		{
			member.childs.emplace_back();
			member.childs[member.childs.size() - 1].isLexem = true;
			member.childs[member.childs.size() - 1].indent = indent;
			member.childs[member.childs.size() - 1].data = allCode[counterNowLexem_line][counterNowLexem_column];

			addColumn(counterNowLexem_line, counterNowLexem_column);

			member.childs.emplace_back();
			if ((error = FLexem::Block(indent, member.childs[member.childs.size() - 1])) != 0)
			{
				return error;
			}

			if (getLexem(counterNowLexem_line, counterNowLexem_column) == true && 
				allCode[counterNowLexem_line][counterNowLexem_column].hashCode == 400)
			{
				member.childs.emplace_back();
				member.childs[member.childs.size() - 1].isLexem = true;
				member.childs[member.childs.size() - 1].indent = indent;
				member.childs[member.childs.size() - 1].data = allCode[counterNowLexem_line][counterNowLexem_column];

				addColumn(counterNowLexem_line, counterNowLexem_column);
				return 0;
			}
			else
			{
				return 2;
			}
		}
		else
		{
			return 2;
		}
	}

	return 1;
}

int FLexem::Block(int indent, appleLexem& member)
{
	member.isLexem = false;
	member.indent = indent;
	member.typeRootName = 3;

	indent += indentSize;
	if (getLexem(counterNowLexem_line, counterNowLexem_column) == true && 
		allCode[counterNowLexem_line][counterNowLexem_column].hashCode == 302)
	{
		member.childs.emplace_back();
		member.childs[member.childs.size() - 1].isLexem = true;
		member.childs[member.childs.size() - 1].indent = indent;
		member.childs[member.childs.size() - 1].data = allCode[counterNowLexem_line][counterNowLexem_column];

		addColumn(counterNowLexem_line, counterNowLexem_column);

		member.childs.emplace_back();
		FLexem::StatementsList(indent, member.childs[member.childs.size() - 1]);

		if (getLexem(counterNowLexem_line, counterNowLexem_column) == true && 
			allCode[counterNowLexem_line][counterNowLexem_column].hashCode == 303)
		{
			member.childs.emplace_back();
			member.childs[member.childs.size() - 1].isLexem = false;
			member.childs[member.childs.size() - 1].indent = indent + indentSize;
			member.childs[member.childs.size() - 1].typeRootName = 13;

			member.childs.emplace_back();
			member.childs[member.childs.size() - 1].isLexem = true;
			member.childs[member.childs.size() - 1].indent = indent;
			member.childs[member.childs.size() - 1].data = allCode[counterNowLexem_line][counterNowLexem_column];

			addColumn(counterNowLexem_line, counterNowLexem_column);
			return 0;
		}
		else
		{
			return 6;
		}
	}
	else
	{
		return 5;
	}
}

int FLexem::StatementsList(int indent, appleLexem& member)
{
	member.isLexem = false;
	member.indent = indent;
	member.typeRootName = 4;

	indent += indentSize;
	return 0;
}

int FLexem::ParametrsList(int indent, appleLexem& member)
{
	member.isLexem = false;
	member.indent = indent;
	member.typeRootName = 5;

	indent += indentSize;
	int error = 0;

	if (getLexem(counterNowLexem_line, counterNowLexem_column) == true && 
		allCode[counterNowLexem_line][counterNowLexem_column].hashCode == 410)
	{
		member.childs.emplace_back();
		member.childs[member.childs.size() - 1].isLexem = true;
		member.childs[member.childs.size() - 1].indent = indent;
		member.childs[member.childs.size() - 1].data = allCode[counterNowLexem_line][counterNowLexem_column];

		addColumn(counterNowLexem_line, counterNowLexem_column);

		member.childs.emplace_back();
		if ((error = FLexem::DeclarationsList(indent, member.childs[member.childs.size() - 1])) != 0)
		{
			return error;
		}

		if (getLexem(counterNowLexem_line, counterNowLexem_column) == true && 
			allCode[counterNowLexem_line][counterNowLexem_column].hashCode == 411)
		{
			member.childs.emplace_back();
			member.childs[member.childs.size() - 1].isLexem = true;
			member.childs[member.childs.size() - 1].indent = indent;
			member.childs[member.childs.size() - 1].data = allCode[counterNowLexem_line][counterNowLexem_column];

			addColumn(counterNowLexem_line, counterNowLexem_column);
			return 0;
		}
		else
		{
			return 7;
		}
	}

	member.childs.emplace_back();
	member.childs[member.childs.size() - 1].isLexem = false;
	member.childs[member.childs.size() - 1].indent = indent;
	member.childs[member.childs.size() - 1].typeRootName = 13;

	return 0;
}

int FLexem::DeclarationsList(int indent, appleLexem& member)
{
	member.isLexem = false;
	member.indent = indent;
	member.typeRootName = 6;

	indent += indentSize;
	int error = 0;

	member.childs.emplace_back();
	if ((error = FLexem::Declaration(indent, member.childs[member.childs.size() - 1])) != -1)
	{

		if (error == 0)
		{
			member.childs.emplace_back();
			if ((error = FLexem::DeclarationsList(indent, member.childs[member.childs.size() - 1])) == 0)
			{
				return 0;
			}
			else
			{
				return error;
			}
		}
		else
		{
			return error;
		}
	}
	else
	{
		member.childs.pop_back();

		member.childs.emplace_back();
		member.childs[member.childs.size() - 1].isLexem = false;
		member.childs[member.childs.size() - 1].indent = indent;
		member.childs[member.childs.size() - 1].typeRootName = 13;

		return 0;
	}
}

int FLexem::Declaration(int indent, appleLexem& member)
{
	member.isLexem = false;
	member.indent = indent;
	member.typeRootName = 7;

	indent += indentSize;
	int error = 0;

	member.childs.emplace_back();
	if (FLexem::VariableIdentifier(indent, member.childs[member.childs.size() - 1]) == false)
	{
		return -1;
	}

	member.childs.emplace_back();
	if ((error = FLexem::IdentifiersList(indent, member.childs[member.childs.size() - 1])) == 0)
	{
		if (getLexem(counterNowLexem_line, counterNowLexem_column) == true &&
			allCode[counterNowLexem_line][counterNowLexem_column].hashCode == 402)
		{
			member.childs.emplace_back();
			member.childs[member.childs.size() - 1].isLexem = true;
			member.childs[member.childs.size() - 1].indent = indent;
			member.childs[member.childs.size() - 1].data = allCode[counterNowLexem_line][counterNowLexem_column];

			addColumn(counterNowLexem_line, counterNowLexem_column);

			member.childs.emplace_back();
			if (FLexem::Attribute(indent, member.childs[member.childs.size() - 1]) == false)
			{
				return 10;
			}

			member.childs.emplace_back();
			FLexem::AttributesList(indent, member.childs[member.childs.size() - 1]);

			if (getLexem(counterNowLexem_line, counterNowLexem_column) == true &&
				allCode[counterNowLexem_line][counterNowLexem_column].hashCode == 400)
			{
				member.childs.emplace_back();
				member.childs[member.childs.size() - 1].isLexem = true;
				member.childs[member.childs.size() - 1].indent = indent;
				member.childs[member.childs.size() - 1].data = allCode[counterNowLexem_line][counterNowLexem_column];

				addColumn(counterNowLexem_line, counterNowLexem_column);

				return 0;
			}
			else
			{
				return 2;
			}
		}
		else
		{
			return 9;
		}
	}
	else
	{
		return error;
	}

	if (getLexem(counterNowLexem_line, counterNowLexem_column) == true &&
		allCode[counterNowLexem_line][counterNowLexem_column].type == TYPE_IDENTIFIER)
	{
		member.childs.emplace_back();
		member.childs[member.childs.size() - 1].isLexem = true;
		member.childs[member.childs.size() - 1].indent = indent;
		member.childs[member.childs.size() - 1].data = allCode[counterNowLexem_line][counterNowLexem_column];

		addColumn(counterNowLexem_line, counterNowLexem_column);
		return true;
	}
	return false;
}

int FLexem::IdentifiersList(int indent, appleLexem& member)
{
	member.isLexem = false;
	member.indent = indent;
	member.typeRootName = 8;

	indent += indentSize;

	if (getLexem(counterNowLexem_line, counterNowLexem_column) == true &&
		allCode[counterNowLexem_line][counterNowLexem_column].hashCode == 403)
	{
		member.childs.emplace_back();
		member.childs[member.childs.size() - 1].isLexem = true;
		member.childs[member.childs.size() - 1].indent = indent;
		member.childs[member.childs.size() - 1].data = allCode[counterNowLexem_line][counterNowLexem_column];

		addColumn(counterNowLexem_line, counterNowLexem_column);

		member.childs.emplace_back();
		if (FLexem::VariableIdentifier(indent, member.childs[member.childs.size() - 1]) == false)
		{
			return 4;
		}

		member.childs.emplace_back();
		return FLexem::IdentifiersList(indent, member.childs[member.childs.size() - 1]);
	}
	else
	{
		member.childs.emplace_back();
		member.childs[member.childs.size() - 1].isLexem = false;
		member.childs[member.childs.size() - 1].indent = indent;
		member.childs[member.childs.size() - 1].typeRootName = 13;
	}
	return 0;
}

int FLexem::AttributesList(int indent, appleLexem& member)
{
	member.isLexem = false;
	member.indent = indent;
	member.typeRootName = 9;

	indent += indentSize;

	member.childs.emplace_back();
	if (FLexem::Attribute(indent, member.childs[member.childs.size() - 1]) == true)
	{
		member.childs.emplace_back();
		return FLexem::AttributesList(indent, member.childs[member.childs.size() - 1]);
	}
	else
	{
		member.childs.pop_back();

		member.childs.emplace_back();
		member.childs[member.childs.size() - 1].isLexem = false;
		member.childs[member.childs.size() - 1].indent = indent;
		member.childs[member.childs.size() - 1].typeRootName = 13;

		return 0;
	}
}

bool FLexem::Attribute(int indent, appleLexem& member)
{
	member.isLexem = false;
	member.indent = indent;
	member.typeRootName = 10;

	indent += indentSize;
	if (getLexem(counterNowLexem_line, counterNowLexem_column) == true &&
		allCode[counterNowLexem_line][counterNowLexem_column].type == TYPE_ATTRIBUTES)
	{
		member.childs.emplace_back();
		member.childs[member.childs.size() - 1].isLexem = true;
		member.childs[member.childs.size() - 1].indent = indent;
		member.childs[member.childs.size() - 1].data = allCode[counterNowLexem_line][counterNowLexem_column];

		addColumn(counterNowLexem_line, counterNowLexem_column);
		return true;
	}
	else
	{
		member.childs.emplace_back();
		member.childs[member.childs.size() - 1].isLexem = false;
		member.childs[member.childs.size() - 1].indent = indent;
		member.childs[member.childs.size() - 1].typeRootName = 13;

		return false;
	}
}

bool FLexem::ProcedureIdentifier(int indent, appleLexem& member)
{
	member.isLexem = false;
	member.indent = indent;
	member.typeRootName = 11;

	indent += indentSize;
	if (getLexem(counterNowLexem_line, counterNowLexem_column) == true &&
		allCode[counterNowLexem_line][counterNowLexem_column].type == TYPE_IDENTIFIER)
	{
		member.childs.emplace_back();
		member.childs[member.childs.size() - 1].isLexem = true;
		member.childs[member.childs.size() - 1].indent = indent;
		member.childs[member.childs.size() - 1].data = allCode[counterNowLexem_line][counterNowLexem_column];

		addColumn(counterNowLexem_line, counterNowLexem_column);
		return true;
	}
	else
	{
		member.childs.emplace_back();
		member.childs[member.childs.size() - 1].isLexem = false;
		member.childs[member.childs.size() - 1].indent = indent;
		member.childs[member.childs.size() - 1].typeRootName = 13;

		return false;
	}
}

bool FLexem::VariableIdentifier(int indent, appleLexem& member)
{
	member.isLexem = false;
	member.indent = indent;
	member.typeRootName = 12;

	indent += indentSize;
	if (getLexem(counterNowLexem_line, counterNowLexem_column) == true &&
		allCode[counterNowLexem_line][counterNowLexem_column].type == TYPE_IDENTIFIER)
	{
		member.childs.emplace_back();
		member.childs[member.childs.size() - 1].isLexem = true;
		member.childs[member.childs.size() - 1].indent = indent;
		member.childs[member.childs.size() - 1].data = allCode[counterNowLexem_line][counterNowLexem_column];

		addColumn(counterNowLexem_line, counterNowLexem_column);
		return true;
	}
	else
	{
		member.childs.emplace_back();
		member.childs[member.childs.size() - 1].isLexem = false;
		member.childs[member.childs.size() - 1].indent = indent;
		member.childs[member.childs.size() - 1].typeRootName = 13;

		return false;
	}
}
